package com.streams.samples.payment.gateway.bridge.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * A container for all events required to process payments.
 */
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class PaymentProcessingContainer {
    @NonNull
    private PaymentReceived paymentReceived;
    @NonNull
    private FraudCheckCompleted fraudCheckCompleted;
}
