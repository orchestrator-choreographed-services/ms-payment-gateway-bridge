package com.streams.samples.payment.gateway.bridge.model;

import com.streams.samples.payment.gateway.bridge.common.Currency;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
public class PaymentReceived {
    @NonNull
    private String fromAccount;
    @NonNull
    private String toAccount;
    @NonNull
    private String amount;
    @NonNull
    private Currency currency;
    @NonNull
    private String paymentId;
}
