package com.streams.samples.payment.gateway.bridge.model;

import lombok.NonNull;
import lombok.Value;

@Value
public class PaymentProcessed {
    //This field is added only to support passing constant value to Zeebe. Zeebe connector supports extracting values from
    //fields, it does not support passing constant value
    @NonNull
    private final String paymentId;
    //Same as paymentId. This field is added only to support Zeebe connector
    @NonNull
    private final String eventType = "PaymentProcessed";
}
