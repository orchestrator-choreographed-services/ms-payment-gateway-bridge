package com.streams.samples.payment.gateway.bridge.model;

public enum FraudCheckResultStatus {
    Accepted,
    Hold,
    Rejected;
}
