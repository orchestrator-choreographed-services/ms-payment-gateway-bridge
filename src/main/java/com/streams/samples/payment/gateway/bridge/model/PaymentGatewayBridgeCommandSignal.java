package com.streams.samples.payment.gateway.bridge.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.NonNull;
import lombok.Value;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentGatewayBridgeCommandSignal {
    private String commandPayload;

    public PaymentGatewayBridgeCommandSignal() {
    }

    public PaymentGatewayBridgeCommandSignal(String commandPayload) {
        this.commandPayload = commandPayload;
    }

    public String getCommandPayload() {
        return commandPayload;
    }

    public void setCommandPayload(String commandPayload) {
        this.commandPayload = commandPayload;
    }
}
