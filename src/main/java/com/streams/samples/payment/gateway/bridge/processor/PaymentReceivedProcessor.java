package com.streams.samples.payment.gateway.bridge.processor;

import com.streams.samples.payment.gateway.bridge.core.PaymentGatewayBridge;
import com.streams.samples.payment.gateway.bridge.model.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.Joined;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.messaging.handler.annotation.SendTo;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

@Slf4j
@EnableBinding(PaymentReceivedProcessor.StreamProcessor.class)
public class PaymentReceivedProcessor {
    @Autowired
    private PaymentGatewayBridge paymentGatewayBridge;

    @StreamListener
    @SendTo("paymentProcessed")
    public KStream<String, PaymentProcessed> process(@Input("paymentReceived") KStream<String, PaymentReceived> paymentReceived,
                                                     @Input("fraudCheckCompleted") KStream<String, FraudCheckCompleted> fraudCheckCompleted,
                                                     @Input("paymentGatewayBridgeCommandSignal") KStream<String, PaymentGatewayBridgeCommandSignal> commandSignal) {

        paymentReceived.peek((k, v) -> log.info("Received a payment received event with payment ID {}", k));
        commandSignal.peek((k, v) -> log.info("Received a command signal with payment ID {}", k));
        fraudCheckCompleted.peek((k, v) -> log.info("Received a fraud check completed event with payment ID {}", k));

        var paymentReadyForProcessingStream = joinPaymentReceivedWithFraudResult(paymentReceived, fraudCheckCompleted);
        paymentReadyForProcessingStream.peek((k, v) -> log.info("Payment with ID {} is ready to be processed with fraud check result status of {}",
                k, v.getFraudCheckCompleted().getResultStatus()));

        return paymentReadyForProcessingStream.join(commandSignal, (v1, v2) -> v1, joinWindow(), joinedWithCommandSignal())
                .mapValues((k, v) -> paymentGatewayBridge.sendPayment(k, v));
    }

    private KStream<String, PaymentProcessingContainer> joinPaymentReceivedWithFraudResult(KStream<String, PaymentReceived> paymentReceived,
                                                                                           KStream<String, FraudCheckCompleted> fraudCheckCompleted) {
        var joined = Joined.with(new Serdes.StringSerde(), new JsonSerde<>(PaymentReceived.class), new JsonSerde<>(FraudCheckCompleted.class));
        return paymentReceived.join(fraudCheckCompleted, PaymentProcessingContainer::new, joinWindow(), joined);
    }

    private JoinWindows joinWindow() {
        return JoinWindows.of(Duration.of(1, ChronoUnit.HOURS).toMillis());
    }

    private Joined<String, PaymentProcessingContainer, PaymentGatewayBridgeCommandSignal> joinedWithCommandSignal() {
        return Joined.with(new Serdes.StringSerde(), new JsonSerde<>(PaymentProcessingContainer.class), new JsonSerde<>(PaymentGatewayBridgeCommandSignal.class));
    }

    public interface StreamProcessor {
        @Input("paymentReceived")
        KStream<?, ?> paymentReceived();

        @Input("paymentGatewayBridgeCommandSignal")
        KStream<?, ?> paymentGatewayBridgeCommandSignal();

        @Input("fraudCheckCompleted")
        KStream<?, ?> fraudCheckCompleted();

        @Output("paymentProcessed")
        KStream<?, ?> paymentProcessed();
    }
}
