package com.streams.samples.payment.gateway.bridge.core;

import com.streams.samples.payment.gateway.bridge.model.PaymentProcessed;
import com.streams.samples.payment.gateway.bridge.model.PaymentProcessingContainer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class PaymentGatewayBridge {
    public PaymentProcessed sendPayment(String paymentId, PaymentProcessingContainer paymentProcessingContainer) {
        log.info("Sending a payment with ID {} to gateway", paymentId);
        return new PaymentProcessed(paymentId);
    }
}
