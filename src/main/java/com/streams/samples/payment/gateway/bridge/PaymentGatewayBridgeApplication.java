package com.streams.samples.payment.gateway.bridge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentGatewayBridgeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentGatewayBridgeApplication.class, args);
	}

}
