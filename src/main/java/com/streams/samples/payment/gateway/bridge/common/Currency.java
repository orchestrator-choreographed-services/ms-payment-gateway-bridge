package com.streams.samples.payment.gateway.bridge.common;

public enum Currency {
    AUD,
    USD,
    EGP
}
