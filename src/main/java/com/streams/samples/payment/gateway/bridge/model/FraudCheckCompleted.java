package com.streams.samples.payment.gateway.bridge.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class FraudCheckCompleted {
    @NonNull
    private String paymentId;
    @NonNull
    private String eventType;
    @NonNull
    private FraudCheckResultStatus resultStatus;
}
